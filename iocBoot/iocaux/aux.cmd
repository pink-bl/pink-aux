#!../../bin/linux-x86_64/aux

## You may have to change aux to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/aux.dbd"
aux_registerRecordDeviceDriver pdbbase

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "100100")

## Load record instances
dbLoadRecords("db/aux.db","BL=PINK, DEV=AUX")
dbLoadRecords("db/auxcaenels.db","BL=PINK, DEV=AUX")
dbLoadRecords("db/gapset.db","BL=PINK, DEV=AUX")

cd "${TOP}/iocBoot/${IOC}"

## Load record instances
dbLoadRecords("clk.db","BL=PINK, DEV=CLK")
dbLoadRecords("display.db","BL=PINK, DEV=DISPLAY")
dbLoadRecords("pshellbuff.db","BL=PINK, DEV=STS")
dbLoadRecords("gapaux.db","BL=PINK, DEV=AUX")

## autosave
set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")
set_pass1_restoreFile("auto_settings.sav")

iocInit

## autosave
create_monitor_set("auto_settings.req", 30, "BL=PINK")

## Start any sequence programs
#seq sncxxx,"user=epics"
