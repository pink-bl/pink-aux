# pink-aux

IOC with some support PVs for PINK beamline

**Docker tags**

v1.10:

- added FFT python script and supplimentary PVs for handling the FFT data
- changed the ssh rout to pink-nuc02

v1.11:

- adjusted fft algorithm